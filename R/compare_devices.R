# Test ride 1 ------------------------------------------------------------

#' Private method for plot_compare_devices_test_ride_1()
#'
#' @param section_name Name of the section
#' @param fpp_section Section data of the FPP
#' @param comfortbike_section Section data of the Comfortbike
#' @param drivenby_section Section data of the device from Drivenby
#'
#' @return 2 Plots
#' @keywords internal
.plot_sections_test_ride_1 <-
  function(section_name,
           fpp_section,
           comfortbike_section,
           drivenby_section) {
    fpp_section@test_ride_1 <-
      fpp_section@test_ride_1[1:(nrow(fpp_section@test_ride_1) - 1), ]

    if (section_name != fpp_section@name ||
      section_name != comfortbike_section@name ||
      section_name != comfortbike_section@name) {
      return("error")
    }

    min_y <- min(
      c(
        fpp_section@test_ride_1$fpp_cv_0_5,
        comfortbike_section@test_ride_1$score_vibr,
        drivenby_section@test_ride_1$RAW.RRUF.V.T
      )
    )
    max_y <- max(
      c(
        fpp_section@test_ride_1$fpp_cv_0_5,
        comfortbike_section@test_ride_1$score_vibr,
        drivenby_section@test_ride_1$RAW.RRUF.V.T
      )
    )

    # Plot score by measurements [12.5m interval] ----------------------------

    min_x <- min(
      c(
        fpp_section@test_ride_1$point,
        comfortbike_section@test_ride_1$point,
        drivenby_section@test_ride_1$point
      )
    )
    max_x <- max(
      c(
        fpp_section@test_ride_1$point,
        comfortbike_section@test_ride_1$point,
        drivenby_section@test_ride_1$point
      )
    )

    graphics::plot(
      1,
      type = "n",
      main = paste(section_name, ": Test ride 1"),
      xlab = "Measurements",
      ylab = "Score",
      xlim = c(min_x, max_x),
      ylim = c(min_y, max_y)
    )

    # FPP
    graphics::lines(
      fpp_cv_0_5 ~ point,
      data = fpp_section@test_ride_1,
      col = fpp_section@col_1,
      lwd = 1,
      type = "l"
    )

    # Comfortbike
    graphics::lines(
      score_vibr ~ point,
      data = comfortbike_section@test_ride_1,
      col = comfortbike_section@col_1,
      lwd = 1,
      type = "l"
    )

    # Drivenby
    graphics::lines(
      RAW.RRUF.V.T ~ point,
      data = drivenby_section@test_ride_1,
      col = drivenby_section@col_1,
      lwd = 1,
      type = "l"
    )

    graphics::legend(
      "topleft",
      legend = c(
        paste(fpp_section@method, " [fpp_cv_0_5]"),
        paste(comfortbike_section@method, " [score_vibr]"),
        paste(drivenby_section@method, " (1/[RAW.RRUF.V]*40)")
      ),
      col = c(
        fpp_section@col_1,
        comfortbike_section@col_1,
        drivenby_section@col_1
      ),
      lty = c(1, 1),
      cex = 0.8,
      text.font = 4,
      lwd = 2
    )


    # Plot score by GPS distance --------------------------------------------------

    min_x <- min(
      c(
        fpp_section@test_ride_1$st_distance,
        comfortbike_section@test_ride_1$st_distance,
        drivenby_section@test_ride_1$st_distance
      )
    )
    max_x <- max(
      c(
        fpp_section@test_ride_1$st_distance,
        comfortbike_section@test_ride_1$st_distance,
        drivenby_section@test_ride_1$st_distance
      )
    )

    graphics::plot(
      1,
      type = "n",
      main = paste(section_name, ": Test ride 1"),
      xlab = "Meter",
      ylab = "Score",
      xlim = c(min_x, max_x),
      ylim = c(min_y, max_y)
    )

    # FPP
    graphics::lines(
      fpp_cv_0_5 ~ st_distance,
      data = fpp_section@test_ride_1,
      col = fpp_section@col_1,
      lwd = 1,
      type = "l"
    )

    # Comfortbike
    graphics::lines(
      comfortbike_section@test_ride_1$score_vibr ~ st_distance,
      data = comfortbike_section@test_ride_1,
      col = comfortbike_section@col_1,
      lwd = 1,
      type = "l"
    )


    # Drivenby
    graphics::lines(
      RAW.RRUF.V.T ~ st_distance,
      data = drivenby_section@test_ride_1,
      col = drivenby_section@col_1,
      lwd = 1,
      type = "l"
    )

    graphics::legend(
      "topleft",
      legend = c(
        paste(fpp_section@method, " [fpp_cv_0_5]"),
        paste(comfortbike_section@method, " [score_vibr]"),
        paste(drivenby_section@method, " (1/[RAW.RRUF.V]*40)")
      ),
      col = c(
        fpp_section@col_1,
        comfortbike_section@col_1,
        drivenby_section@col_1
      ),
      lty = c(1, 1),
      cex = 0.8,
      text.font = 4,
      lwd = 2
    )
  }

#' Compare devices
#'
#' Plots the calculated data from all devices in one diagram. This is done for
#' both visualization methods (in sequence and using the distance from start point
#' with GPS coordinates)
#'
#' @return 10 Plots
#' @export
#' @examples
#' plot_compare_devices_test_ride_1()
plot_compare_devices_test_ride_1 <- function() {
  .plot_sections_test_ride_1(
    "Section 1",
    CyclePathSurfaceQuality::fpp_section_1,
    CyclePathSurfaceQuality::comfortbike_section_1,
    CyclePathSurfaceQuality::drivenby_section_1
  )
  .plot_sections_test_ride_1(
    "Section 2",
    CyclePathSurfaceQuality::fpp_section_2,
    CyclePathSurfaceQuality::comfortbike_section_2,
    CyclePathSurfaceQuality::drivenby_section_2
  )
  .plot_sections_test_ride_1(
    "Section 3",
    CyclePathSurfaceQuality::fpp_section_3,
    CyclePathSurfaceQuality::comfortbike_section_3,
    CyclePathSurfaceQuality::drivenby_section_3
  )
  .plot_sections_test_ride_1(
    "Section 4",
    CyclePathSurfaceQuality::fpp_section_4,
    CyclePathSurfaceQuality::comfortbike_section_4,
    CyclePathSurfaceQuality::drivenby_section_4
  )
  .plot_sections_test_ride_1(
    "Section 5",
    CyclePathSurfaceQuality::fpp_section_5,
    CyclePathSurfaceQuality::comfortbike_section_5,
    CyclePathSurfaceQuality::drivenby_section_5
  )
}

# Test ride 2 ------------------------------------------------------------

#' Private method for plot_compare_devices_test_ride_2()
#'
#' @param section_name Name of the section
#' @param fpp_section Section data of the FPP
#' @param comfortbike_section Section data of the Comfortbike
#' @param drivenby_section Section data of the device from Drivenby
#'
#' @return 2 Plots
#' @keywords internal
.plot_sections_test_ride_2 <-
  function(section_name,
           fpp_section,
           comfortbike_section,
           drivenby_section) {
    fpp_section@test_ride_2 <-
      fpp_section@test_ride_2[1:(nrow(fpp_section@test_ride_2) - 1), ]

    if (section_name != fpp_section@name ||
      section_name != comfortbike_section@name ||
      section_name != comfortbike_section@name) {
      return("error")
    }

    min_y <- min(
      c(
        fpp_section@test_ride_2$fpp_cv_0_5,
        comfortbike_section@test_ride_2$score_vibr,
        drivenby_section@test_ride_2$RAW.RRUF.V.T
      )
    )
    max_y <- max(
      c(
        fpp_section@test_ride_2$fpp_cv_0_5,
        comfortbike_section@test_ride_2$score_vibr,
        drivenby_section@test_ride_2$RAW.RRUF.V.T
      )
    )


    # Plot score by measurements [12.5m interval] ----------------------------------------------

    min_x <- min(
      c(
        fpp_section@test_ride_2$point,
        comfortbike_section@test_ride_2$point,
        drivenby_section@test_ride_2$point
      )
    )
    max_x <- max(
      c(
        fpp_section@test_ride_2$point,
        comfortbike_section@test_ride_2$point,
        drivenby_section@test_ride_2$point
      )
    )

    graphics::plot(
      1,
      type = "n",
      main = paste(section_name, ": Test ride 2"),
      xlab = "Measurements",
      ylab = "Score",
      xlim = c(min_x, max_x),
      ylim = c(min_y, max_y)
    )

    # FPP
    graphics::lines(
      fpp_cv_0_5 ~ point,
      data = fpp_section@test_ride_2,
      col = fpp_section@col_2,
      lwd = 1,
      type = "l"
    )

    # Comfortbike
    graphics::lines(
      score_vibr ~ point,
      data = comfortbike_section@test_ride_2,
      col = comfortbike_section@col_2,
      lwd = 1,
      type = "l"
    )

    # Drivenby
    graphics::lines(
      RAW.RRUF.V.T ~ point,
      data = drivenby_section@test_ride_2,
      col = drivenby_section@col_2,
      lwd = 1,
      type = "l"
    )

    graphics::legend(
      "topleft",
      legend = c(
        paste(fpp_section@method, " [fpp_cv_0_5]"),
        paste(comfortbike_section@method, " [score_vibr]"),
        paste(drivenby_section@method, " (1/[RAW.RRUF.V]*40)")
      ),
      col = c(
        fpp_section@col_2,
        comfortbike_section@col_2,
        drivenby_section@col_2
      ),
      lty = c(1, 1),
      cex = 0.8,
      text.font = 4,
      lwd = 2
    )

    # Plot score by GPS distance ------------------------------------

    min_x <- min(
      c(
        fpp_section@test_ride_2$st_distance,
        comfortbike_section@test_ride_2$st_distance,
        drivenby_section@test_ride_2$st_distance
      )
    )
    max_x <- max(
      c(
        fpp_section@test_ride_2$st_distance,
        comfortbike_section@test_ride_2$st_distance,
        drivenby_section@test_ride_2$st_distance
      )
    )

    graphics::plot(
      1,
      type = "n",
      main = paste(section_name, ": Test ride 2"),
      xlab = "Meter",
      ylab = "Score",
      xlim = c(min_x, max_x),
      ylim = c(min_y, max_y)
    )

    # FPP
    graphics::lines(
      fpp_cv_0_5 ~ st_distance,
      data = fpp_section@test_ride_2,
      col = fpp_section@col_2,
      lwd = 1,
      type = "l"
    )

    # Comfortbike
    lines(
      comfortbike_section@test_ride_2$score_vibr ~ st_distance,
      data = comfortbike_section@test_ride_2,
      col = comfortbike_section@col_2,
      lwd = 1,
      type = "l"
    )

    # Drivenby
    graphics::lines(
      RAW.RRUF.V.T ~ st_distance,
      data = drivenby_section@test_ride_2,
      col = drivenby_section@col_2,
      lwd = 1,
      type = "l"
    )

    graphics::legend(
      "topleft",
      legend = c(
        paste(fpp_section@method, " [fpp_cv_0_5]"),
        paste(comfortbike_section@method, " [score_vibr]"),
        paste(drivenby_section@method, " (1/[RAW.RRUF.V]*40)")
      ),
      col = c(
        fpp_section@col_2,
        comfortbike_section@col_2,
        drivenby_section@col_2
      ),
      lty = c(1, 1),
      cex = 0.8,
      text.font = 4,
      lwd = 2
    )
  }



#' Compare devices
#'
#' Plots the calculated data from all devices in one diagram. This is done for
#' both visualization methods (in sequence and using the distance from start point
#' with GPS coordinates)
#'
#' @return 10 Plots
#' @export
#'
#' @examples
#' plot_compare_devices_test_ride_2()
plot_compare_devices_test_ride_2 <- function() {
  .plot_sections_test_ride_2(
    "Section 1",
    CyclePathSurfaceQuality::fpp_section_1,
    CyclePathSurfaceQuality::comfortbike_section_1,
    CyclePathSurfaceQuality::drivenby_section_1
  )
  .plot_sections_test_ride_2(
    "Section 2",
    CyclePathSurfaceQuality::fpp_section_2,
    CyclePathSurfaceQuality::comfortbike_section_2,
    CyclePathSurfaceQuality::drivenby_section_2
  )
  .plot_sections_test_ride_2(
    "Section 3",
    CyclePathSurfaceQuality::fpp_section_3,
    CyclePathSurfaceQuality::comfortbike_section_3,
    CyclePathSurfaceQuality::drivenby_section_3
  )
  .plot_sections_test_ride_2(
    "Section 4",
    CyclePathSurfaceQuality::fpp_section_4,
    CyclePathSurfaceQuality::comfortbike_section_4,
    CyclePathSurfaceQuality::drivenby_section_4
  )
  .plot_sections_test_ride_2(
    "Section 5",
    CyclePathSurfaceQuality::fpp_section_5,
    CyclePathSurfaceQuality::comfortbike_section_5,
    CyclePathSurfaceQuality::drivenby_section_5
  )
}
