#' Information about the measured segments
#'
#' The method returns statistical information (quantile, median, mean,
#' standard derivation, confidence interval) about the measured segments
#' of the three devices (FPP, Comfortbike, Drivenby).
#' @return data.frame
#'
#' @importFrom stats sd
#' @importFrom stats t.test
#' @export
#'
#' @examples
#' get_segment_info()
get_segment_info <- function() {
  lengths_fpp <- get_segment_lengths_fpp()
  lengths_comfortbike <- get_segment_lengths_comfortbike()
  lengths_drivenby <- get_segment_lengths_drivenby()

  # FPP
  conf_interval_fpp <- stats::t.test(lengths_fpp, conf.level = 0.95, mu = 12.5)

  length_stats_fpp <- c(summary(lengths_fpp),
    sd = stats::sd(lengths_fpp),
    conf_95_1 = conf_interval_fpp$conf.int[1],
    conf_95_2 = conf_interval_fpp$conf.int[2]
  )

  # Comfortbike
  conf_interval_comfortbike <- t.test(lengths_comfortbike,
    conf.level = 0.95, mu = 12.5
  )

  length_stats_comfortbike <- c(summary(lengths_comfortbike),
    sd = sd(lengths_comfortbike),
    conf_95_1 = conf_interval_comfortbike$conf.int[1],
    conf_95_2 = conf_interval_comfortbike$conf.int[2]
  )

  # Drivenby
  conf_interval_drivenby <- t.test(lengths_drivenby, conf.level = 0.95)

  length_stats_drivenby <- c(summary(lengths_drivenby),
    sd = sd(lengths_drivenby),
    conf_95_1 = conf_interval_drivenby$conf.int[1],
    conf_95_1 = conf_interval_drivenby$conf.int[2]
  )


  # All together
  length_stats <- t(data.frame(
    fpp = length_stats_fpp,
    comfortbike = length_stats_comfortbike,
    drivenby = length_stats_drivenby
  ))

  return(length_stats)
}
