# FPP --------------------------

#' Returns the data of the FPP sorted by the surface type.Road sections
#' that have multiple surface types are divided at the points where the measurements
#' show a greater change in the data.
#' @param fpp_sections section data. Vector with all five fpp sections. Needs to be sorted:
#' fpp_section_1, fpp_section2,...
#' @return Data of FPP separated by road surface type.
#' @export
#'
#' @examples
#' get_surface_data_fpp()
#' get_surface_data_fpp(c(
#'   CyclePathSurfaceQuality::fpp_section_1,
#'   CyclePathSurfaceQuality::fpp_section_2,
#'   CyclePathSurfaceQuality::fpp_section_3,
#'   CyclePathSurfaceQuality::fpp_section_4,
#'   CyclePathSurfaceQuality::fpp_section_5
#' ))
get_surface_data_fpp <- function(fpp_sections) {
  surface_data <- c()

  sec_1_split_ride_1 <- 29
  sec_1_split_ride_2 <- 29

  sec_5_split_1_ride_1 <- 15
  sec_5_split_1_ride_2 <- 15

  sec_5_split_2_ride_1 <- 34
  sec_5_split_2_ride_2 <- 34

  ### Handle data input
  if (missing(fpp_sections)) {
    fpp_section_1 <- CyclePathSurfaceQuality::fpp_section_1
    fpp_section_2 <- CyclePathSurfaceQuality::fpp_section_2
    fpp_section_3 <- CyclePathSurfaceQuality::fpp_section_3
    fpp_section_4 <- CyclePathSurfaceQuality::fpp_section_4
    fpp_section_5 <- CyclePathSurfaceQuality::fpp_section_5
  } else {
    if (length(fpp_sections) != 5) {
      stop("wrong number of parameter")
    }
    fpp_section_1 <- fpp_sections[[1]]
    fpp_section_2 <- fpp_sections[[2]]
    fpp_section_3 <- fpp_sections[[3]]
    fpp_section_4 <- fpp_sections[[4]]
    fpp_section_5 <- fpp_sections[[5]]
  }


  ### Order data
  fpp_section_1@test_ride_1 <- fpp_section_1@test_ride_1[order(fpp_section_1@test_ride_1$point), ]
  fpp_section_1@test_ride_2 <- fpp_section_1@test_ride_2[order(fpp_section_1@test_ride_2$point), ]

  fpp_section_2@test_ride_1 <- fpp_section_2@test_ride_1[order(fpp_section_2@test_ride_1$point), ]
  fpp_section_2@test_ride_2 <- fpp_section_2@test_ride_2[order(fpp_section_2@test_ride_2$point), ]

  fpp_section_3@test_ride_1 <- fpp_section_3@test_ride_1[order(fpp_section_3@test_ride_1$point), ]
  fpp_section_3@test_ride_2 <- fpp_section_3@test_ride_2[order(fpp_section_3@test_ride_2$point), ]

  fpp_section_4@test_ride_1 <- fpp_section_4@test_ride_1[order(fpp_section_4@test_ride_1$point), ]
  fpp_section_4@test_ride_2 <- fpp_section_4@test_ride_2[order(fpp_section_4@test_ride_2$point), ]

  fpp_section_5@test_ride_1 <- fpp_section_5@test_ride_1[order(fpp_section_5@test_ride_1$point), ]
  fpp_section_5@test_ride_2 <- fpp_section_5@test_ride_2[order(fpp_section_5@test_ride_2$point), ]

  ## Concrete
  surface_data$concrete$sec_1_ride_1 <- fpp_section_1@test_ride_1[c(1:sec_1_split_ride_1), ]
  surface_data$concrete$sec_1_ride_2 <- fpp_section_1@test_ride_2[c(1:sec_1_split_ride_2), ]

  surface_data$concrete$sec_2_ride_1 <- fpp_section_2@test_ride_1
  surface_data$concrete$sec_2_ride_2 <- fpp_section_2@test_ride_2

  surface_data$concrete$sec_5_ride_1 <- fpp_section_5@test_ride_1[c((sec_5_split_1_ride_1 + 1):(sec_5_split_2_ride_1)), ]
  surface_data$concrete$sec_5_ride_2 <- fpp_section_5@test_ride_2[c((sec_5_split_1_ride_2 + 1):(sec_5_split_2_ride_2)), ]

  ## Concrete tiles
  surface_data$concrete_tiles$sec_1_ride_1 <- fpp_section_1@test_ride_1[c((sec_1_split_ride_1 + 1):nrow(fpp_section_1@test_ride_1)), ]
  surface_data$concrete_tiles$sec_1_ride_2 <- fpp_section_1@test_ride_2[c((sec_1_split_ride_2 + 1):nrow(fpp_section_1@test_ride_2)), ]

  surface_data$concrete_tiles$sec_4_ride_1 <- fpp_section_4@test_ride_1
  surface_data$concrete_tiles$sec_4_ride_2 <- fpp_section_4@test_ride_2

  ## Asphalt
  surface_data$asphalt$sec_3_ride_1 <- fpp_section_3@test_ride_1
  surface_data$asphalt$sec_3_ride_2 <- fpp_section_3@test_ride_2

  surface_data$asphalt$sec_5_1_ride_1 <- fpp_section_5@test_ride_1[c(1:sec_5_split_1_ride_1), ]
  surface_data$asphalt$sec_5_1_ride_2 <- fpp_section_5@test_ride_2[c(1:sec_5_split_1_ride_2), ]

  surface_data$asphalt$sec_5_2_ride_1 <- fpp_section_5@test_ride_1[c((sec_5_split_2_ride_1 + 1):nrow(fpp_section_5@test_ride_1)), ]
  surface_data$asphalt$sec_5_2_ride_2 <- fpp_section_5@test_ride_2[c((sec_5_split_2_ride_2 + 1):nrow(fpp_section_5@test_ride_2)), ]



  surface_data
}


# Comfortbike --------------------------

#' Returns the data of the Comfortbike sorted by the surface type. Road sections
#' that have multiple surface types are divided at the points where the measurements
#' show a greater change in the data.
#' @param comfortbike_sections section data. Vector with all five comfortbike sections. Needs to be sorted:
#' comfortbike_section_1, comfortbike_section2,...
#' @return Data of Comfortbike separated by road surface type.
#' @export
#'
#' @examples
#' get_surface_data_comfortbike()
#' get_surface_data_comfortbike(c(
#'   CyclePathSurfaceQuality::comfortbike_section_1,
#'   CyclePathSurfaceQuality::comfortbike_section_2,
#'   CyclePathSurfaceQuality::comfortbike_section_3,
#'   CyclePathSurfaceQuality::comfortbike_section_4,
#'   CyclePathSurfaceQuality::comfortbike_section_5
#' ))
get_surface_data_comfortbike <- function(comfortbike_sections) {
  surface_data <- c()

  sec_1_split_ride_1 <- 31
  sec_1_split_ride_2 <- 30

  sec_5_split_1_ride_1 <- 15
  sec_5_split_1_ride_2 <- 15

  sec_5_split_2_ride_1 <- 36
  sec_5_split_2_ride_2 <- 36

  ### Handle data input
  if (missing(comfortbike_sections)) {
    comfortbike_section_1 <- CyclePathSurfaceQuality::comfortbike_section_1
    comfortbike_section_2 <- CyclePathSurfaceQuality::comfortbike_section_2
    comfortbike_section_3 <- CyclePathSurfaceQuality::comfortbike_section_3
    comfortbike_section_4 <- CyclePathSurfaceQuality::comfortbike_section_4
    comfortbike_section_5 <- CyclePathSurfaceQuality::comfortbike_section_5
  } else {
    if (length(comfortbike_sections) != 5) {
      stop("wrong number of parameter")
    }
    comfortbike_section_1 <- comfortbike_sections[[1]]
    comfortbike_section_2 <- comfortbike_sections[[2]]
    comfortbike_section_3 <- comfortbike_sections[[3]]
    comfortbike_section_4 <- comfortbike_sections[[4]]
    comfortbike_section_5 <- comfortbike_sections[[5]]
  }

  ### Order data
  comfortbike_section_1@test_ride_1 <- comfortbike_section_1@test_ride_1[order(comfortbike_section_1@test_ride_1$point), ]
  comfortbike_section_1@test_ride_2 <- comfortbike_section_1@test_ride_2[order(comfortbike_section_1@test_ride_2$point), ]

  comfortbike_section_2@test_ride_1 <- comfortbike_section_2@test_ride_1[order(comfortbike_section_2@test_ride_1$point), ]
  comfortbike_section_2@test_ride_2 <- comfortbike_section_2@test_ride_2[order(comfortbike_section_2@test_ride_2$point), ]

  comfortbike_section_3@test_ride_1 <- comfortbike_section_3@test_ride_1[order(comfortbike_section_3@test_ride_1$point), ]
  comfortbike_section_3@test_ride_2 <- comfortbike_section_3@test_ride_2[order(comfortbike_section_3@test_ride_2$point), ]

  comfortbike_section_4@test_ride_1 <- comfortbike_section_4@test_ride_1[order(comfortbike_section_4@test_ride_1$point), ]
  comfortbike_section_4@test_ride_2 <- comfortbike_section_4@test_ride_2[order(comfortbike_section_4@test_ride_2$point), ]

  comfortbike_section_5@test_ride_1 <- comfortbike_section_5@test_ride_1[order(comfortbike_section_5@test_ride_1$point), ]
  comfortbike_section_5@test_ride_2 <- comfortbike_section_5@test_ride_2[order(comfortbike_section_5@test_ride_2$point), ]

  ## Concrete
  surface_data$concrete$sec_1_ride_1 <- comfortbike_section_1@test_ride_1[c(1:sec_1_split_ride_1), ]
  surface_data$concrete$sec_1_ride_2 <- comfortbike_section_1@test_ride_2[c(1:sec_1_split_ride_2), ]

  surface_data$concrete$sec_2_ride_1 <- comfortbike_section_2@test_ride_1
  surface_data$concrete$sec_2_ride_2 <- comfortbike_section_2@test_ride_2

  surface_data$concrete$sec_5_ride_1 <- comfortbike_section_5@test_ride_1[c((sec_5_split_1_ride_1 + 1):(sec_5_split_2_ride_1)), ]
  surface_data$concrete$sec_5_ride_2 <- comfortbike_section_5@test_ride_2[c((sec_5_split_1_ride_2 + 1):(sec_5_split_2_ride_2)), ]

  ## Concrete tiles
  surface_data$concrete_tiles$sec_1_ride_1 <- comfortbike_section_1@test_ride_1[c((sec_1_split_ride_1 + 1):nrow(comfortbike_section_1@test_ride_1)), ]
  surface_data$concrete_tiles$sec_1_ride_2 <- comfortbike_section_1@test_ride_2[c((sec_1_split_ride_2 + 1):nrow(comfortbike_section_1@test_ride_2)), ]

  surface_data$concrete_tiles$sec_4_ride_1 <- comfortbike_section_4@test_ride_1
  surface_data$concrete_tiles$sec_4_ride_2 <- comfortbike_section_4@test_ride_2

  ## Asphalt
  surface_data$asphalt$sec_3_ride_1 <- comfortbike_section_3@test_ride_1
  surface_data$asphalt$sec_3_ride_2 <- comfortbike_section_3@test_ride_2

  surface_data$asphalt$sec_5_1_ride_1 <- comfortbike_section_5@test_ride_1[c(1:sec_5_split_1_ride_1), ]
  surface_data$asphalt$sec_5_1_ride_2 <- comfortbike_section_5@test_ride_2[c(1:sec_5_split_1_ride_2), ]

  surface_data$asphalt$sec_5_2_ride_1 <- comfortbike_section_5@test_ride_1[c((sec_5_split_2_ride_1 + 1):nrow(comfortbike_section_5@test_ride_1)), ]
  surface_data$asphalt$sec_5_2_ride_2 <- comfortbike_section_5@test_ride_2[c((sec_5_split_2_ride_2 + 1):nrow(comfortbike_section_5@test_ride_2)), ]



  surface_data
}


# Drivenby--------------------------

#' Returns the data of the device from Drivenby sorted by the surface type.Road sections
#' that have multiple surface types are divided at the points where the measurements
#' show a greater change in the data.
#'
#' @param drivenby_sections section data. Vector with all five device from drivenby sections. Needs to be sorted:
#' drivenby_section_1, drivenby_section2,...
#' @return Data from the device from Drivenby separated by road surface type.
#' @export
#'
#' @examples
#' get_surface_data_drivenby()
#' get_surface_data_drivenby(c(
#'   CyclePathSurfaceQuality::drivenby_section_1,
#'   CyclePathSurfaceQuality::drivenby_section_2,
#'   CyclePathSurfaceQuality::drivenby_section_3,
#'   CyclePathSurfaceQuality::drivenby_section_4,
#'   CyclePathSurfaceQuality::drivenby_section_5
#' ))
get_surface_data_drivenby <- function(drivenby_sections) {
  surface_data <- c()

  sec_1_split_ride_1 <- 28
  sec_1_split_ride_2 <- 28

  sec_5_split_1_ride_1 <- 11
  sec_5_split_1_ride_2 <- 14

  sec_5_split_2_ride_1 <- 32
  sec_5_split_2_ride_2 <- 34

  ### Handle data input
  if (missing(drivenby_sections)) {
    drivenby_section_1 <- CyclePathSurfaceQuality::drivenby_section_1
    drivenby_section_2 <- CyclePathSurfaceQuality::drivenby_section_2
    drivenby_section_3 <- CyclePathSurfaceQuality::drivenby_section_3
    drivenby_section_4 <- CyclePathSurfaceQuality::drivenby_section_4
    drivenby_section_5 <- CyclePathSurfaceQuality::drivenby_section_5
  } else {
    if (length(drivenby_sections) != 5) {
      stop("wrong number of parameter")
    }
    drivenby_section_1 <- drivenby_sections[[1]]
    drivenby_section_2 <- drivenby_sections[[2]]
    drivenby_section_3 <- drivenby_sections[[3]]
    drivenby_section_4 <- drivenby_sections[[4]]
    drivenby_section_5 <- drivenby_sections[[5]]
  }

  ### Order data
  drivenby_section_1@test_ride_1 <- drivenby_section_1@test_ride_1[order(drivenby_section_1@test_ride_1$point), ]
  drivenby_section_1@test_ride_2 <- drivenby_section_1@test_ride_2[order(drivenby_section_1@test_ride_2$point), ]

  drivenby_section_2@test_ride_1 <- drivenby_section_2@test_ride_1[order(drivenby_section_2@test_ride_1$point), ]
  drivenby_section_2@test_ride_2 <- drivenby_section_2@test_ride_2[order(drivenby_section_2@test_ride_2$point), ]

  drivenby_section_3@test_ride_1 <- drivenby_section_3@test_ride_1[order(drivenby_section_3@test_ride_1$point), ]
  drivenby_section_3@test_ride_2 <- drivenby_section_3@test_ride_2[order(drivenby_section_3@test_ride_2$point), ]

  drivenby_section_4@test_ride_1 <- drivenby_section_4@test_ride_1[order(drivenby_section_4@test_ride_1$point), ]
  drivenby_section_4@test_ride_2 <- drivenby_section_4@test_ride_2[order(drivenby_section_4@test_ride_2$point), ]

  drivenby_section_5@test_ride_1 <- drivenby_section_5@test_ride_1[order(drivenby_section_5@test_ride_1$point), ]
  drivenby_section_5@test_ride_2 <- drivenby_section_5@test_ride_2[order(drivenby_section_5@test_ride_2$point), ]

  ## Concrete
  surface_data$concrete$sec_1_ride_1 <- drivenby_section_1@test_ride_1[c(1:sec_1_split_ride_1), ]
  surface_data$concrete$sec_1_ride_2 <- drivenby_section_1@test_ride_2[c(1:sec_1_split_ride_2), ]

  surface_data$concrete$sec_2_ride_1 <- drivenby_section_2@test_ride_1
  surface_data$concrete$sec_2_ride_2 <- drivenby_section_2@test_ride_2

  surface_data$concrete$sec_5_ride_1 <- drivenby_section_5@test_ride_1[c((sec_5_split_1_ride_1 + 1):(sec_5_split_2_ride_1)), ]
  surface_data$concrete$sec_5_ride_2 <- drivenby_section_5@test_ride_2[c((sec_5_split_1_ride_2 + 1):(sec_5_split_2_ride_2)), ]

  ## Concrete tiles
  surface_data$concrete_tiles$sec_1_ride_1 <- drivenby_section_1@test_ride_1[c((sec_1_split_ride_1 + 1):nrow(drivenby_section_1@test_ride_1)), ]
  surface_data$concrete_tiles$sec_1_ride_2 <- drivenby_section_1@test_ride_2[c((sec_1_split_ride_2 + 1):nrow(drivenby_section_1@test_ride_2)), ]

  surface_data$concrete_tiles$sec_4_ride_1 <- drivenby_section_4@test_ride_1
  surface_data$concrete_tiles$sec_4_ride_2 <- drivenby_section_4@test_ride_2

  ## Asphalt
  surface_data$asphalt$sec_3_ride_1 <- drivenby_section_3@test_ride_1
  surface_data$asphalt$sec_3_ride_2 <- drivenby_section_3@test_ride_2

  surface_data$asphalt$sec_5_1_ride_1 <- drivenby_section_5@test_ride_1[c(1:sec_5_split_1_ride_1), ]
  surface_data$asphalt$sec_5_1_ride_2 <- drivenby_section_5@test_ride_2[c(1:sec_5_split_1_ride_2), ]

  surface_data$asphalt$sec_5_2_ride_1 <- drivenby_section_5@test_ride_1[c((sec_5_split_2_ride_1 + 1):nrow(drivenby_section_5@test_ride_1)), ]
  surface_data$asphalt$sec_5_2_ride_2 <- drivenby_section_5@test_ride_2[c((sec_5_split_2_ride_2 + 1):nrow(drivenby_section_5@test_ride_2)), ]



  surface_data
}
# Execute to test
# data<-get_surface_data_drivenby()
