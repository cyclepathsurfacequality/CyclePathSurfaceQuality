---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/",
  out.width = "100%"
)
```

# CyclePathSurfaceQuality

<!-- badges: start -->

<!-- badges: end -->

The goal of the package CyclePathSurfaceQuality is to do research about the possibility to transform data from different technologies into a uniform scale.

The first part of the research has been published in the paper "Analysis of the measurements of cycle path surface quality as collected by three different technologies" at the EMOS Conference 2022. The paper summarizes the research that is included in package version 0.1.0. The package provides further results and give access to the code and the data. The version v0.1.0 is
stored also in a separate branch: EMOS2022 (<https://gitlab.uni-oldenburg.de/cyclepathsurfacequality/CyclePathSurfaceQuality/-/tree/EMOS2022/>).


```{r child = 'README_content.Rmd'}
```
