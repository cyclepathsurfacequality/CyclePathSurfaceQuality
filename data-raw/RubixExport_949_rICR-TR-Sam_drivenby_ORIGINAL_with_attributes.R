# Code to prepare `RubixExport_949_rICR-TR-Sam_drivenby_ORIGINAL_with_attributes` dataset goes here
drivenby <- sf::st_read("./data-raw/RubixExport_949_rICR-TR-Sam_drivenby_ORIGINAL_with_attributes.shp")
drivenby <- drivenby[is.na(drivenby$comp_key) == 0, ]

# Put Data into a nicer data structure

# Test ride 1
drivenby_section_1 <-
  CyclePathSurfaceQuality::create_section(
    "Section 1", 1, "Drivenby", drivenby[drivenby$section == 1, ], "green",
    "darkgreen"
  )
# Test ride 2
drivenby_section_2 <-
  CyclePathSurfaceQuality::create_section(
    "Section 2", 2, "Drivenby", drivenby[drivenby$section == 2, ], "green",
    "darkgreen"
  )
# Test ride 3
drivenby_section_3 <-
  CyclePathSurfaceQuality::create_section(
    "Section 3", 3, "Drivenby", drivenby[drivenby$section == 3, ], "green",
    "darkgreen"
  )
# Test ride 4
drivenby_section_4 <-
  CyclePathSurfaceQuality::create_section(
    "Section 4", 4, "Drivenby", drivenby[drivenby$section == 4, ], "green",
    "darkgreen"
  )
# Test ride 5
drivenby_section_5 <-
  CyclePathSurfaceQuality::create_section(
    "Section 5", 5, "Drivenby", drivenby[drivenby$section == 5, ], "green",
    "darkgreen"
  )


# Calculate distances and lengths for Drivenby

.calculateDrivenbyDistances <- function(drivenby_section) {
  # The first measurement is recorded after 12.5 m.
  st_distances_test_1 <- units::as_units(0, "m")
  st_lengths_test_1 <- units::as_units(0, "m")
  comp_key_test_1 <- c()

  st_distances_test_2 <- units::as_units(0, "m")
  st_lengths_test_2 <- units::as_units(0, "m")
  comp_key_test_2 <- c()


  # Test ride 1
  for (val in 1:nrow(drivenby_section@test_ride_1)) {
    calculated_distance <- 0

    if (val > 1) {
      # if, because the trip was recorded in the opposite direction
      if (drivenby_section@section_nbr == 1) {
        coordinates <- sf::st_coordinates(drivenby_section@test_ride_1[val - 1, ])[1, ]
        last_point <-
          sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)

        coordinates <- sf::st_coordinates(drivenby_section@test_ride_1[val, ])[2, ]
        new_point <-
          sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)
      } else {
        coordinates <- sf::st_coordinates(drivenby_section@test_ride_1[val - 1, ])[2, ]
        last_point <-
          sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)

        coordinates <- sf::st_coordinates(drivenby_section@test_ride_1[val, ])[1, ]
        new_point <-
          sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)
      }


      calculated_distance <-
        sf::st_distance(last_point, new_point) + sf::st_length(drivenby_section@test_ride_1[val, ])
    } else {
      calculated_distance <- sf::st_length(drivenby_section@test_ride_1[val, ])
    }

    st_lengths_test_1 <- append(st_lengths_test_1, calculated_distance)

    # Add calculated distance for new point and the comp_key from that point
    # to the corresponding lists
    st_distances_test_1 <-
      append(
        st_distances_test_1,
        st_distances_test_1[val] + calculated_distance
      )
    comp_key_test_1 <-
      append(comp_key_test_1, drivenby_section@test_ride_1[val, ]$comp_key)
  }


  # Test ride 2
  for (val in 1:nrow(drivenby_section@test_ride_2)) {
    calculated_distance <- 0
    if (val > 1) {
      coordinates <- sf::st_coordinates(drivenby_section@test_ride_2[val - 1, ])[2, ]
      last_point <-
        sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)

      coordinates <- sf::st_coordinates(drivenby_section@test_ride_2[val, ])[1, ]
      new_point <-
        sf::st_sfc(sf::st_point(c(coordinates[1], coordinates[2]))) %>% sf::st_set_crs(4326)

      calculated_distance <-
        sf::st_distance(last_point, new_point) + sf::st_length(drivenby_section@test_ride_2[val, ])
    } else {
      calculated_distance <- sf::st_length(drivenby_section@test_ride_2[val, ])
    }

    st_lengths_test_2 <- append(st_lengths_test_2, calculated_distance)

    # Add calculated distance for new point and the comp_key from that point
    # to the cosponding lists
    st_distances_test_2 <-
      append(
        st_distances_test_2,
        st_distances_test_2[val] + calculated_distance
      )
    comp_key_test_2 <-
      append(comp_key_test_2, drivenby_section@test_ride_2[val, ]$comp_key)
  }

  # Create a dataframe where all distances and comp_keys are stored
  calculated_distance_and_keys <-
    data.frame(
      st_distance = c(
        st_distances_test_1[2:length(st_distances_test_1)],
        st_distances_test_2[2:length(st_distances_test_2)]
      ),
      length = c(
        st_lengths_test_1[2:length(st_lengths_test_1)],
        st_lengths_test_2[2:length(st_lengths_test_2)]
      ),
      comp_key = c(comp_key_test_1, comp_key_test_2)
    )

  # Add the distances to the data frame using the comp_key for the join
  drivenby_section@all_data <- drivenby_section@all_data %>%
    dplyr::left_join(calculated_distance_and_keys)

  # Update the data for test ride 1
  if (drivenby_section@section_nbr == 1) {
    drivenby_section@test_ride_1 <-
      drivenby_section@all_data[drivenby_section@all_data$test_ride == 3, ]
    drivenby_section@test_ride_1 <-
      drivenby_section@test_ride_1[order(drivenby_section@test_ride_1$point), ]
  } else {
    drivenby_section@test_ride_1 <-
      drivenby_section@all_data[drivenby_section@all_data$test_ride == 1, ]
    drivenby_section@test_ride_1 <-
      drivenby_section@test_ride_1[order(drivenby_section@test_ride_1$point), ]
  }

  # Update the data for test ride 2
  drivenby_section@test_ride_2 <-
    drivenby_section@all_data[drivenby_section@all_data$test_ride == 2, ]
  drivenby_section@test_ride_2 <-
    drivenby_section@test_ride_2[order(drivenby_section@test_ride_2$point), ]

  return(drivenby_section)
}


methods::setGeneric(name = "calculateDrivenbyDistances", function(drivenby_section) {
  standardGeneric("calculateDrivenbyDistances")
})
methods::setMethod(
  "calculateDrivenbyDistances",
  methods::signature("Section"),
  .calculateDrivenbyDistances
)


drivenby_section_1 <- calculateDrivenbyDistances(drivenby_section_1)
drivenby_section_2 <- calculateDrivenbyDistances(drivenby_section_2)
drivenby_section_3 <- calculateDrivenbyDistances(drivenby_section_3)
drivenby_section_4 <- calculateDrivenbyDistances(drivenby_section_4)
drivenby_section_5 <- calculateDrivenbyDistances(drivenby_section_5)


#' Transform Raw.Ruf.V data
#'
#' To compare the data of the device from Drivenby the data are scaled and shifted so that
#' they can be better visualized with the values of the other devices.
#'
#' @param drivenby_section
#'
#' @return
#' @keywords internal
#' @examples
.transform_RAW.RRUF.V <- function(drivenby_section) {
  drivenby_section@test_ride_1$RAW.RRUF.V.T <-
    1 / drivenby_section@test_ride_1$RAW.RRUF.V * 40
  drivenby_section@test_ride_2$RAW.RRUF.V.T <-
    1 / drivenby_section@test_ride_2$RAW.RRUF.V * 40
  return(drivenby_section)
}
methods::setGeneric(name = "transform_RAW.RRUF.V", function(drivenby_section) {
  standardGeneric("transform_RAW.RRUF.V")
})
methods::setMethod(
  "transform_RAW.RRUF.V",
  methods::signature("Section"),
  .transform_RAW.RRUF.V
)

drivenby_section_1 <- transform_RAW.RRUF.V(drivenby_section_1)
drivenby_section_2 <- transform_RAW.RRUF.V(drivenby_section_2)
drivenby_section_3 <- transform_RAW.RRUF.V(drivenby_section_3)
drivenby_section_4 <- transform_RAW.RRUF.V(drivenby_section_4)
drivenby_section_5 <- transform_RAW.RRUF.V(drivenby_section_5)

# Prepare data for all 12.5 meters

drivenby_section_1 <- add_12_5_meter_distance(drivenby_section_1)
drivenby_section_2 <- add_12_5_meter_distance(drivenby_section_2)
drivenby_section_3 <- add_12_5_meter_distance(drivenby_section_3)
drivenby_section_4 <- add_12_5_meter_distance(drivenby_section_4)
drivenby_section_5 <- add_12_5_meter_distance(drivenby_section_5)

# Export the data
usethis::use_data(drivenby_section_1, overwrite = TRUE)
usethis::use_data(drivenby_section_2, overwrite = TRUE)
usethis::use_data(drivenby_section_3, overwrite = TRUE)
usethis::use_data(drivenby_section_4, overwrite = TRUE)
usethis::use_data(drivenby_section_5, overwrite = TRUE)
