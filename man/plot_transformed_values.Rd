% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_transformed_values.R
\name{plot_transformed_values}
\alias{plot_transformed_values}
\title{Plots the transformed data or one section}
\usage{
plot_transformed_values(
  section_data_fpp,
  section_data_comfortbike,
  section_data_drivenby
)
}
\arguments{
\item{section_data_fpp}{Section data of the FPP}

\item{section_data_comfortbike}{Section data of the Comfortbike}

\item{section_data_drivenby}{Section data of the device from Drivenby}
}
\value{
plot
}
\description{
Plots the transformed data or one section
}
\examples{
plot_transformed_values(
  CyclePathSurfaceQuality::fpp_section_1,
  CyclePathSurfaceQuality::comfortbike_section_1,
  CyclePathSurfaceQuality::drivenby_section_1
)
}
