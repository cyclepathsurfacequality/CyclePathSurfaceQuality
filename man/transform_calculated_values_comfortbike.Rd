% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/transform_calculated_values.R
\name{transform_calculated_values_comfortbike}
\alias{transform_calculated_values_comfortbike}
\title{Transform calculated values of the Comfortbike}
\usage{
transform_calculated_values_comfortbike(section_data)
}
\arguments{
\item{section_data}{Data of a road section from the Comfortbike}
}
\value{
Data with new column: score_vibr_transformed
}
\description{
Transforms the calculated values using the data from section 4 to create the
transformation function. Some lower and higher values have been dropped, so that
the data shows a normal distribution within a histogram.
}
\examples{
transform_calculated_values_comfortbike(CyclePathSurfaceQuality::comfortbike_section_1)
}
