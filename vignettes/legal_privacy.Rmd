---
title: "Legal notice and privacy"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Legal notice and privacy}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


__Privacy__

We are very pleased about your interest in our company. Data protection is of a particularly high priority for the management of the Carl von Ossietzky University Oldenburg. The use of the Internet pages of the Carl von Ossietzky Universität Oldenburg is possible without any indication of personal data. However, if a data subject wants to use special services provided by our enterprise via our website, processing of personal data could become necessary. If processing of personal data is necessary and there is no legal basis for such processing, we will generally obtain the consent of the data subject.

The processing of personal data, such as the name, address, e-mail address, or telephone number of a data subject shall always be in line with the General Data Protection Regulation and in accordance with the country-specific data protection provisions applicable to the Carl von Ossietzky University Oldenburg. By means of this data protection declaration, our company would like to inform the public about the type, scope and purpose of the personal data we collect, use and process. Furthermore, data subjects are informed of their rights by means of this data protection declaration.

As the controller, the Carl von Ossietzky Universität Oldenburg has implemented numerous technical and organizational measures to ensure the most complete protection of personal data processed through this website. Nevertheless, Internet-based data transmissions can always be subject to security vulnerabilities, so that absolute protection cannot be guaranteed. For this reason, every data subject is free to transmit personal data to us by alternative means, for example by telephone.


## 1. Definitions

The data protection declaration of the Carl von Ossietzky University Oldenburg is based on the terms used by the European Directive and Ordinance when issuing the General Data Protection Regulation (DS-GVO). Our data protection declaration should be easy to read and understand for the public as well as for our customers and business partners. To ensure this, we would like to explain the terms used in advance.

We use the following terms, among others, in this Privacy Policy:


a) Personal data

Personal data is any information relating to an identified or identifiable natural person (hereinafter “data subject”). An identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.


b) Person concerned

Data subject means any identified or identifiable natural person whose personal data are processed by the controller.


c) Processing

Processing is any operation or set of operations which is performed upon personal data, whether or not by automatic means, such as collection, recording, organization, filing, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction.


d) Restriction of processing

Restriction of processing is the marking of stored personal data with the aim of limiting their future processing.


e) Profiling

Profiling is any type of automated processing of personal data that consists of using such personal data to evaluate certain personal aspects relating to a natural person, in particular to analyze or predict aspects relating to that natural person’s job performance, economic situation, health, personal preferences, interests, reliability, behavior, location or change of location.


f) Pseudonymization

Pseudonymization is the processing of personal data in such a way that the personal data can no longer be attributed to a specific data subject without the use of additional information, provided that such additional information is kept separate and is subject to technical and organizational measures to ensure that the personal data is not attributed to an identified or identifiable natural person.


g) Controller or person responsible for processing

The controller or controller is the natural or legal person, public authority, agency or other body which alone or jointly with others determines the purposes and means of the processing of personal data. Where the purposes and means of such processing are determined by Union or Member State law, the controller or the specific criteria for its designation may be provided for under Union or Member State law.


h) Processor

Processor means a natural or legal person, public authority, agency or other body that processes personal data on behalf of the Controller.


i) Receiver

A recipient is a natural or legal person, public authority, agency or other body to whom personal data are disclosed, whether or not a third party. However, public authorities that may receive personal data in the context of a specific investigation mandate under Union or Member State law shall not be considered as recipients.


j) Third person

Third person means a natural or legal person, public authority, agency or other body other than the data subject, the controller, the processor and the persons who are authorized to process the personal data under the direct responsibility of the controller or the processor.


k) Consent

Consent shall mean any freely given indication of the data subject’s wishes for the specific case in an informed and unambiguous manner in the form of a statement or any other unambiguous affirmative act by which the data subject indicates that he or she consents to the processing of personal data relating to him or her.


## 2. Name and address of the controller

The responsible party within the meaning of the General Data Protection Regulation, other data protection laws applicable in the Member States of the European Union and other provisions of a data protection nature is:

Carl von Ossietzky Universität Oldenburg

Ammerländer Heerstr. 114-118

26129 Oldenburg

Deutschland

Tel.: 0441/798-4183

E-Mail: jorge.marx.gomez@uol.de

## 3. Cookies

The internet pages of the Carl von Ossietzky University of Oldenburg use cookies. Cookies are text files that are placed and stored on a computer system via an Internet browser.

Numerous Internet pages and servers use cookies. Many cookies contain a so-called cookie ID. A cookie ID is a unique identifier of the cookie. It consists of a string of characters by which Internet pages and servers can be assigned to the specific Internet browser in which the cookie was stored. This enables the visited Internet pages and servers to distinguish the individual browser of the data subject from other Internet browsers that contain other cookies. A specific internet browser can be recognized and identified via the unique cookie ID.

Through the use of cookies, the Carl von Ossietzky University Oldenburg can provide the users of this website with more user-friendly services that would not be possible without the cookie setting.

By means of a cookie, the information and offers on our website can be optimized for the user. Cookies enable us, as already mentioned, to recognize the users of our website. The purpose of this recognition is to make it easier for users to use our website. For example, the user of a website that uses cookies does not have to re-enter his or her access data each time he or she visits the website, because this is handled by the website and the cookie stored on the user’s computer system. Another example is the cookie of a shopping cart in an online store. The online store remembers the items that a customer has placed in the virtual shopping cart via a cookie.

The data subject can prevent the setting of cookies by our website at any time by means of an appropriate setting of the Internet browser used and thus permanently object to the setting of cookies. Furthermore, cookies that have already been set can be deleted at any time via an Internet browser or other software programs. This is possible in all common Internet browsers. If the data subject deactivates the setting of cookies in the Internet browser used, not all functions of our website may be fully usable.


## 4. Collection of general data and information

The website of the Carl von Ossietzky University Oldenburg collects a series of general data and information each time a data subject or automated system calls up the website. This general data and information is stored in the log files of the server. The following data may be collected: (1) the browser types and versions used, (2) the operating system used by the accessing system, (3) the website from which an accessing system accesses our website (so-called referrer), (4) the sub-websites that are accessed via an accessing system on our website, (5) the date and time of an access to the website, (6) an Internet protocol address (IP address), (7) the Internet service provider of the accessing system and (8) other similar data and information that serve to avert danger in the event of attacks on our information technology systems.

When using these general data and information, the Carl von Ossietzky University Oldenburg does not draw any conclusions about the data subject. Rather, this information is needed (1) to deliver the content of our website correctly, (2) to optimize the content of our website and the advertising for it, (3) to ensure the long-term functionality of our information technology systems and the technology of our website, and (4) to provide law enforcement authorities with the information necessary for prosecution in the event of a cyber attack. Therefore, the Carl von Ossietzky University Oldenburg analyzes anonymously collected data and information on one hand, and on the other hand, with the aim of increasing the data protection and data security of our enterprise, to ensure an optimal level of protection for the personal data we process. The anonymous data of the server log files are stored separately from any personal data provided by a data subject.


## 5. Comment function in the blog on the website

The Carl von Ossietzky University Oldenburg offers users the possibility to leave individual comments on individual blog contributions on a blog, which is on the website of the controller. A blog is a portal maintained on a website, usually publicly viewable, in which one or more persons, called bloggers or web bloggers, can post articles or write down thoughts in so-called blogposts. The blogposts can usually be commented on by third parties.

If a data subject leaves a comment on the blog published on this website, in addition to the comments left by the data subject, information on the time of comment entry and the user name (pseudonym) chosen by the data subject will be stored and published. Furthermore, the IP address assigned by the Internet service provider (ISP) of the person concerned is also logged. This storage of the IP address is done for security reasons and in case the data subject violates the rights of third parties by posting a comment or posts illegal content. The storage of this personal data is therefore in the controller’s own interest, so that the controller could exculpate itself if necessary in the event of an infringement. There is no disclosure of this collected personal data to third parties, unless such disclosure is required by law or serves the legal defense of the controller.


## 6. Routine deletion and blocking of personal data

The controller shall process and store personal data of the data subject only for the period necessary to achieve the purpose of storage or where provided for by the European Directive and Regulation or other legislator in laws or regulations to which the controller is subject.

If the purpose of storage no longer applies or if a storage period prescribed by the European Directive and Regulation or another competent legislator expires, the personal data will be routinely blocked or deleted in accordance with the statutory provisions.


## 7. Data subject rights

a) Right to confirmation

Every data subject shall have the right, granted by the European Directive and the Regulation, to obtain confirmation from the controller as to whether personal data concerning him or her are being processed. If a data subject wishes to exercise this right, he or she may, at any time, contact any employee of the controller.


b) Right to information

Any person concerned by the processing of personal data has the right granted by the European Directive and Regulation to obtain at any time from the controller, free of charge, information about the personal data stored about him or her and a copy of that information. Furthermore, the European Directive and Regulation has granted the data subject access to the following information:

- the purposes of processing
- the categories of personal data processed
- the recipients or categories of recipients to whom the personal data have been or will be disclosed, in particular in the case of recipients in third countries or international organizations
-if possible, the planned duration for which the personal data will be stored or, if this is not possible, the criteria for determining this duration
- the existence of a right to obtain the rectification or erasure of personal data concerning him or her, or to obtain the - restriction of processing by the controller, or a right to object to such processing
- the existence of a right of appeal to a supervisory authority
- if the personal data are not collected from the data subject: Any available information about the origin of the data
- The existence of automated decision-making, including profiling, pursuant to Article 22(1) and (4) of the GDPR and, at least in these cases, meaningful information about the logic involved and the scope and intended effects of such processing for the data subject.

Furthermore, the data subject shall have the right to obtain information as to whether personal data have been transferred to a third country or to an international organization. If this is the case, the data subject also has the right to obtain information about the appropriate safeguards in connection with the transfer.

If a data subject wishes to exercise this right of access, he or she may, at any time, contact an employee of the controller.


c) Right to rectification

Any person affected by the processing of personal data has the right granted by the European Directive and Regulation to request the immediate rectification of inaccurate personal data concerning him or her. Furthermore, the data subject has the right to request the completion of incomplete personal data – also by means of a supplementary declaration – taking into account the purposes of the processing.

If a data subject wishes to exercise this right to rectify, he or she may, at any time, contact any employee of the controller.


d) Right to erasure (right to be forgotten)

Any person concerned by the processing of personal data has the right, granted by the European Directive and Regulation, to obtain from the controller the erasure without delay of personal data concerning him or her, where one of the following reasons applies and insofar as the processing is not necessary:

- The personal data were collected or otherwise processed for such purposes for which they are no longer necessary.
- The data subject revokes the consent on which the processing was based pursuant to Art. 6(1)(a) DS-GVO or Art. 9(2)(a) DS-GVO and there is no other legal basis for the processing.
- The data subject objects to the processing pursuant to Article 21(1) DS-GVO and there are no overriding legitimate grounds for the processing, or the data subject objects to the processing pursuant to Article 21(2) DS-GVO.
- The personal data have been processed unlawfully.
- The erasure of the personal data is necessary for compliance with a legal obligation under Union or Member State law to which the controller is subject.
- The personal data has been collected in relation to information society services offered pursuant to Article 8(1) of the GDPR.

If one of the aforementioned reasons applies, and a data subject wishes to arrange for the deletion of personal data stored by the Carl von Ossietzky Universität Oldenburg, he or she may, at any time, contact any employee of the controller. The employee of the Carl von Ossietzky Universität Oldenburg will arrange for the erasure request to be complied with immediately.

If the personal data was made public by the Carl von Ossietzky University Oldenburg and our company as the responsible party pursuant to Art. 17 Para. 1 DS-GVO to erase the personal data, the Carl von Ossietzky University Oldenburg shall implement reasonable measures, including technical measures, to monitor the available technology and the cost of implementation in order to inform other data controllers which process the published personal data, that the data subject has requested from those other data controllers the erasure of all links to the personal data or copies or replications of the personal data, unless the processing is necessary. The employee of the Carl von Ossietzky University Oldenburg will arrange the necessary in individual cases.


e) Right to restriction of processing

Any person concerned by the processing of personal data has the right, granted by the European Directive and Regulation, to obtain from the controller the restriction of processing if one of the following conditions is met:

- The accuracy of the personal data is contested by the data subject for a period enabling the controller to verify the accuracy of the personal data.
- The processing is unlawful, the data subject objects to the erasure of the personal data and requests instead the restriction of the use of the personal data.
- The controller no longer needs the personal data for the purposes of the processing, but the data subject needs it for the assertion, exercise or defense of legal claims.
- The data subject has objected to the processing pursuant to Article 21 (1) of the GDPR and it is not yet clear whether the legitimate grounds of the controller override those of the data subject.

If one of the aforementioned conditions is met, and a data subject wishes to request the restriction of personal data stored by the Carl von Ossietzky Universität Oldenburg, he or she may, at any time, contact any employee of the controller. The employee of the Carl von Ossietzky University Oldenburg will arrange the restriction of the processing.


f) Right to data portability

Any person concerned by the processing of personal data has the right, granted by the European Directive and Regulation, to receive the personal data concerning him or her, which have been provided by the data subject to a controller, in a structured, commonly used and machine-readable format. He or she also has the right to transmit this data to another controller without hindrance from the controller to whom the personal data have been provided, provided that the processing is based on consent pursuant to Article 6(1)(a) of the GDPR or Article 9(2)(a) of the GDPR or on a contract pursuant to Article 6(1)(b) of the GDPR and the processing is carried out by automated means, unless the processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller.

Furthermore, when exercising the right to data portability pursuant to Article 20(1) of the GDPR, the data subject shall have the right to obtain that the personal data be transferred directly from one controller to another controller where technically feasible and provided that this does not adversely affect the rights and freedoms of other individuals.

In order to assert the right to data portability, the data subject may at any time contact any employee of the Carl von Ossietzky University Oldenburg.


g) Right to object

Any person affected by the processing of personal data has the right granted by the European Directive and Regulation to object at any time, on grounds relating to his or her particular situation, to the processing of personal data concerning him or her carried out on the basis of Article 6(1)(e) or (f) of the GDPR. This also applies to profiling based on these provisions.

The Carl von Ossietzky University Oldenburg shall no longer process the personal data in the event of the objection, unless we can demonstrate compelling legitimate grounds for the processing which override the interests, rights and freedoms of the data subject, or for the assertion, exercise or defence of legal claims.

If the Carl von Ossietzky University Oldenburg processes personal data for direct marketing purposes, the data subject shall have the right to object at any time to processing of personal data for such marketing. This also applies to profiling insofar as it is related to such direct marketing. If the data subject objects to the Carl von Ossietzky University Oldenburg to the processing for direct marketing purposes, the Carl von Ossietzky University Oldenburg will no longer process the personal data for these purposes.

In addition, the data subject has the right, on grounds relating to his or her particular situation, to object to processing of personal data concerning him or her which is carried out by the Carl von Ossietzky University Oldenburg for scientific or historical research purposes, or for statistical purposes pursuant to Article 89(1) of the DS-GVO, unless such processing is necessary for the performance of a task carried out in the public interest.

In order to exercise the right to object, the data subject may directly contact any employee of the Carl von Ossietzky University Oldenburg or another employee. The data subject is also free to exercise his/her right to object in connection with the use of information society services, notwithstanding Directive 2002/58/EC, by means of automated procedures using technical specifications.


h) Automated decisions in individual cases including profiling

Any data subject concerned by the processing of personal data shall have the right, granted by the European Directive and the Regulation, not to be subject to a decision based solely on automated processing, including profiling, which produces legal effects concerning him or her or similarly significantly affects him or her, unless the decision (1) is necessary for entering into, or the performance of, a contract between the data subject and the controller, or (2) is permitted by Union or Member State law to which the controller is subject, and that law contains suitable measures to safeguard the data subject’s rights and freedoms and legitimate interests, or (3) is based on the data subject’s explicit consent.

If the decision (1) is necessary for entering into, or the performance of, a contract between the data subject and the data controller, or (2) it is made with the data subject’s explicit consent, the Carl von Ossietzky University Oldenburg shall implement suitable measures to safeguard the data subject’s rights and freedoms and legitimate interests, which include at least the right to obtain the data subject’s involvement on the part of the controller, to express his or her point of view and to contest the decision.

If the data subject wishes to exercise the rights concerning automated decisions, he or she may, at any time, contact any employee of the controller.


i) Right to revoke consent under data protection law

Any person affected by the processing of personal data has the right granted by the European Directive and Regulation to withdraw consent to the processing of personal data at any time.

If the data subject wishes to exercise the right to withdraw the consent, he or she may, at any time, contact any employee of the controller.


## 8. Legal basis of the processing

Article 6 I lit. a DS-GVO serves our company as the legal basis for processing operations in which we obtain consent for a specific processing purpose. If the processing of personal data is necessary for the performance of a contract to which the data subject is a party, as is the case, for example, with processing operations that are necessary for the delivery of goods or the provision of another service or consideration, the processing is based on Article 6 I lit. b DS-GVO. The same applies to such processing operations that are necessary for the implementation of pre-contractual measures, for example in cases of inquiries about our products or services. If our company is subject to a legal obligation by which a processing of personal data becomes necessary, such as for the fulfillment of tax obligations, the processing is based on Art. 6 I lit. c DS-GVO. In rare cases, the processing of personal data might become necessary to protect vital interests of the data subject or another natural person. This would be the case, for example, if a visitor were to be injured on our premises and as a result his or her name, age, health insurance data or other vital information had to be passed on to a doctor, hospital or other third party. Then the processing would be based on Art. 6 I lit. d DS-GVO. Finally, processing operations could be based on Art. 6 I lit. f DS-GVO. Processing operations that are not covered by any of the aforementioned legal bases are based on this legal basis if the processing is necessary to protect a legitimate interest of our company or a third party, provided that the interests, fundamental rights and freedoms of the data subject are not overridden. Such processing operations are permitted to us in particular because they were specifically mentioned by the European legislator. In this respect, it took the view that a legitimate interest could be assumed if the data subject is a customer of the controller (recital 47 sentence 2 DS-GVO).


## 9. Legitimate interests in the processing pursued by the controller or a third party

If the processing of personal data is based on Article 6 I lit. f DS-GVO, our legitimate interest is the performance of our business activities for the benefit of the well-being of all our employees and our shareholders.


## 10. Duration for which the personal data are stored

The criterion for the duration of the storage of personal data is the respective statutory retention period. After expiry of the period, the corresponding data will be routinely deleted, provided that they are no longer required for the fulfillment or initiation of the contract.


## 11. Legal or contractual requirements to provide the personal data; necessity for the conclusion of the contract; obligation of the data subject to provide the personal data; possible consequences of non-provision

We inform you that the provision of personal data is sometimes required by law (e.g. tax regulations) or may also result from contractual regulations (e.g. information on the contractual partner). Sometimes, in order to conclude a contract, it may be necessary for a data subject to provide us with personal data that must subsequently be processed by us. For example, the data subject is obliged to provide us with personal data if our company concludes a contract with him or her. Failure to provide the personal data would mean that the contract with the data subject could not be concluded. Before providing personal data by the data subject, the data subject must contact one of our employees. Our employee will explain to the data subject on a case-by-case basis whether the provision of the personal data is required by law or by contract or is necessary for the conclusion of the contract, whether there is an obligation to provide the personal data, and what the consequences of not providing the personal data would be.
