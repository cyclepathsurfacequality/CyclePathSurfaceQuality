---
title: "Road surfaces"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Road surfaces}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(CyclePathSurfaceQuality)
```

In the following the data for each road surface type are analysed. The distribution of the 
data for each device and test ride are visualized in histograms. The data of the concrete tiles 
have for all three devices the most similar shape of a normal distribution. One histogram shows all
values which dropped the lowest 5% and the highest 5% of the data. This subset is used for the
transformation of the data into a uniform scale.

## FPP
```{r fig.height=5, fig.width=7,fig.align='center', out.width='98%'}
plot_hist_surfaces_fpp_concrete()
plot_hist_surfaces_fpp_concrete_tiles()
plot_hist_surfaces_fpp_asphalt()
```


## Comforbike
```{r fig.height=5, fig.width=7, fig.align='center', out.width='98%'}
plot_hist_surfaces_comfortbike_concrete()
plot_hist_surfaces_comfortbike_concrete_tiles()
plot_hist_surfaces_comfortbike_asphalt()
```


## Drivenby
```{r fig.height=5, fig.width=7, out.width='98%',fig.align='center'}
plot_hist_surfaces_drivenby_concrete()
plot_hist_surfaces_drivenby_concrete_tiles()
plot_hist_surfaces_drivenby_asphalt()
```


